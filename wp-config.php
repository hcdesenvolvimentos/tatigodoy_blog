<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'tatigodo_blog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'tatigodo_blog');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'tg@2015');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-2E2Nm|Tg*JqQe5!/:z$lp3dU4ESt7ZVgh4?&J68]XNG7qj{th$$b~1d3&8[FR-v');
define('SECURE_AUTH_KEY',  'XuX}jydb)M7yHMa2X^f2kVF0WGIpbn~&|=Jxo4fe1pT}#cBHsN/vm!)o)*v+xRoM');
define('LOGGED_IN_KEY',    ',M{a<Rjtn)5:Q`}53|3N$>aV@)jD?H,Q@{0EaItUL/FBAKu7]aM`pW;)(au`aUBT');
define('NONCE_KEY',        '1EPe@]Ut5EAar}Z&H;@-x*^rQ6es^)#IF^BCbc|Y?tzI<kJx]FB]?7bQ`/Wi&o%{');
define('AUTH_SALT',        '{)x6!?S&RNAR;?k?&8CwF6b^cX_8Z}wWf6!!QMY]/qoY./:gCZL;BKS)T}M%zn:Q');
define('SECURE_AUTH_SALT', 'gO5YOnsZt{q& 2u1Qy$hMFMOX;>)j*L`>#pz}oxp-<adzV;.*DQ!zPKaIcTv^!EQ');
define('LOGGED_IN_SALT',   'QdQ E2&S=%c3o|FR!@XEsjR2_!f9dSkxO+?>l+]mX^B%_lua}k&JhpX2=p.]02W9');
define('NONCE_SALT',       'y3PU@C% _!0XfyyE~/I0)3TxDMSGc=aml,PNG%aZECNb9K1H&&5? oc?,/@;G&Jh');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
define ('WP_MEMORY_LIMIT', '256M');
/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
