<?php

/**
 @wordpress-plugin
 * Plugin Name: Tati Godoy Blog
 * Version:     0.1
 * Plugin URI:  http://hcdesenvolvimentos.com.br 
 * Description: Controle base do tema Tati Godoy Blog.
 * Author:       Agência HC Desenvolvimentos
 * Author URI:   http://hcdesenvolvimentos.com.br 
 * Text Domain: hcdesenvolvimentos-base
 * Domain Path: /languages/
 * License:     GPL2
 */


function baseTatigodoy () {

	// TIPOS DE CONTEÚDO
	conteudosTatigodoy();

	taxonomiaTatigodoy();

	metaboxesTatigodoy();
}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosTatigodoy (){

		// TIPOS DE CONTEÚDO
		// tipoDestaque();

		tipoDepoimentos();	

		tipoBannerDestaque();

		//tipoSobre();

		tipoBannerDestaqueEstanteVirtual();

		tipoEbooksExclusivos();

		tipoDownloadsDiversos();

		//tipoVideosRodape();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
				$titulo = 'Título do destaque';
				break;

				case 'team':
				$titulo = 'Enter member name here';
				break;
				
				default:
				break;
			}

			return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoBannerDestaque() {

		$rotulosDestaque = array(
			'name'               => 'Destaques',
			'singular_name'      => 'destaque',
			'menu_name'          => 'Destaques',
			'name_admin_bar'     => 'Destaques',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaque 	= array(
			'labels'             => $rotulosDestaque,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-images-alt',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaque' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	function tipoDepoimentos() {

		$rotulosDepoimentos = array(
			'name'               => 'Depoimentos',
			'singular_name'      => 'Depoimento',
			'menu_name'          => 'Depoimentos',
			'name_admin_bar'     => 'Depoimentos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo depoimento',
			'new_item'           => 'Novo depoimento',
			'edit_item'          => 'Editar depoimento',
			'view_item'          => 'Ver depoimento',
			'all_items'          => 'Todos os depoimentos',
			'search_items'       => 'Buscar depoimento',
			'parent_item_colon'  => 'Dos depoimentos',
			'not_found'          => 'Nenhum depoimento cadastrado.',
			'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
		);

		$argsDepoimentos 	= array(
			'labels'             => $rotulosDepoimentos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-format-quote',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'depoimentos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('depoimentos', $argsDepoimentos);

	}
	
	function tipoSobre(){
		$rotulosSobre = array(
			'name'               => 'Sobre a Tati',
			'singular_name'      => 'Sobre a Tati',
			'menu_name'          => 'Sobre a Tati',
			'name_admin_bar'     => 'Sobre a Tati',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar nova descrição',
			//'new_item'           => 'Novo sobre',
			'edit_item'          => 'Editar sobre',
			'view_item'          => 'Ver sobre',
			//'all_items'          => 'Todos os depoimentos',
			//'search_items'       => 'Buscar depoimento',
			//'parent_item_colon'  => 'Dos depoimentos',
			'not_found'          => 'Nenhum sobre cadastrado.',
			'not_found_in_trash' => 'Nenhum sobre na lixeira.'
		);

		$argsSobre 	= array(
			'labels'             => $rotulosSobre,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-admin-users',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'sobre' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','editor')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('sobre', $argsSobre);
	}

	function tipoBannerDestaqueEstanteVirtual() {

		$rotulosDestaqueEstanteVirtual = array(
			'name'               => 'Destaques Estante Virtual',
			'singular_name'      => 'destaque estante virtual',
			'menu_name'          => 'Destaques Estante Virtual',
			'name_admin_bar'     => 'Destaques Estante Virtual',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo destaque',
			'new_item'           => 'Novo destaque',
			'edit_item'          => 'Editar destaque',
			'view_item'          => 'Ver destaque',
			'all_items'          => 'Todos os destaques',
			'search_items'       => 'Buscar destaque',
			'parent_item_colon'  => 'Dos destaques',
			'not_found'          => 'Nenhum destaque cadastrado.',
			'not_found_in_trash' => 'Nenhum destaque na lixeira.'
		);

		$argsDestaqueEstanteVirtual 	= array(
			'labels'             => $rotulosDestaqueEstanteVirtual,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-images-alt',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'destaque-estante-virtual' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque-estante', $argsDestaqueEstanteVirtual);

	}

	function tipoEbooksExclusivos() {

		$rotuloEbookExclusivo = array(
			'name'               => 'E-books Exclusivos',
			'singular_name'      => 'E-book exclusivo',
			'menu_name'          => 'E-books Exclusivos',
			'name_admin_bar'     => 'E-books Exclusivos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo e-book exclusivo',
			'new_item'           => 'Novo e-book exclusivo',
			'edit_item'          => 'Editar e-book exclusivo',
			'view_item'          => 'Ver e-book exclusivo',
			'all_items'          => 'Todos os e-books exclusivos',
			'search_items'       => 'Buscar e-book exclusivo',
			'parent_item_colon'  => 'Dos e-books exclusivos',
			'not_found'          => 'Nenhum e-book exclusivo cadastrado.',
			'not_found_in_trash' => 'Nenhum e-book exclusivo na lixeira.'
		);

		$argsEbooksExclusivos 	= array(
			'labels'             => $rotuloEbookExclusivo,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-book',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'ebooks-exclusivos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('ebooks-exclusivos', $argsEbooksExclusivos);

	}

	function tipoDownloadsDiversos() {

		$rotulosDownloadsDiversos = array(
			'name'               => 'E-books diversos',
			'singular_name'      => 'E-book diverso',
			'menu_name'          => 'E-books diversos',
			'name_admin_bar'     => 'E-books diversos',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo e-book',
			'new_item'           => 'Novo e-book',
			'edit_item'          => 'Editar e-book',
			'view_item'          => 'Ver e-book',
			'all_items'          => 'Todos os E-books',
			'search_items'       => 'Buscar e-book',
			'parent_item_colon'  => 'Dos e-books diversos',
			'not_found'          => 'Nenhum e-book cadastrado.',
			'not_found_in_trash' => 'Nenhum e-book na lixeira.'
		);

		$argsDownloadsDiversos 	= array(
			'labels'             => $rotulosDownloadsDiversos,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-download',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'ebooks-diversos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title','thumbnail')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('downloads-diversos', $argsDownloadsDiversos);

	}

	function tipoVideosRodape() {

		$rotulosVideosRodape = array(
			'name'               => 'Vídeos Rodapé',
			'singular_name'      => 'Vídeo Rodapé',
			'menu_name'          => 'Vídeos Rodapé',
			'name_admin_bar'     => 'Vídeos Rodapé',
			'add_new'            => 'Adicionar novo',
			'add_new_item'       => 'Adicionar novo vídeo',
			'new_item'           => 'Novo vídeo',
			'edit_item'          => 'Editar vídeo',
			'view_item'          => 'Ver vídeo',
			'all_items'          => 'Todos os vídeos',
			'search_items'       => 'Buscar vídeo',
			'parent_item_colon'  => 'Dos vídeos',
			'not_found'          => 'Nenhum vídeo cadastrado.',
			'not_found_in_trash' => 'Nenhum vídeo na lixeira.'
		);

		$argsVideosRodape 	= array(
			'labels'             => $rotulosVideosRodape,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'menu_position'		 => 4,
			'menu_icon'          => 'dashicons-video-alt3',
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'videosRodape' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title')
		);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('videosRodape', $argsVideosRodape);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaTatigodoy () {		
		//taxonomiaCategoriaDestaque();
		taxonomiaEbooksExclusivos();

		taxonomiaDownloadsDiversos();
	}
		// TAXONOMIA DE DESTAQUE
	function taxonomiaCategoriaDestaque() {

		$rotulosCategoriaDestaque = array(
			'name'              => 'Categorias de destaque',
			'singular_name'     => 'Categoria de destaque',
			'search_items'      => 'Buscar categorias de destaque',
			'all_items'         => 'Todas as categorias de destaque',
			'parent_item'       => 'Categoria de destaque pai',
			'parent_item_colon' => 'Categoria de destaque pai:',
			'edit_item'         => 'Editar categoria de destaque',
			'update_item'       => 'Atualizar categoria de destaque',
			'add_new_item'      => 'Nova categoria de destaque',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de destaque',
		);

		$argsCategoriaDestaque 		= array(
			'hierarchical'      => true,
			'labels'            => $rotulosCategoriaDestaque,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'categoria-destaque' ),
		);

		register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

	}	
	function taxonomiaEbooksExclusivos() {

		$rotulosTaxonomiaEbooksExclusivos = array(
			'name'              => 'Autor',
			'singular_name'     => 'Categoria de autor',
			'search_items'      => 'Buscar categorias de autor',
			'all_items'         => 'Todas as categorias de autor',
			'parent_item'       => 'Categoria de autor pai',
			'parent_item_colon' => 'Categoria de autor pai:',
			'edit_item'         => 'Editar categoria de autor',
			'update_item'       => 'Atualizar categoria de autor',
			'add_new_item'      => 'Nova categoria de autor',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de autor',
		);

		$argsTaxonomiaEbooksExclusivos   = array(
			'hierarchical'      => true,
			'labels'            => $rotulosTaxonomiaEbooksExclusivos,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'autores-ebooks-exclusivos' ),
		);

		register_taxonomy( 'categoriaEbooksExclusivos', array( 'ebooks-exclusivos' ), $argsTaxonomiaEbooksExclusivos );

	}		
	function taxonomiaDownloadsDiversos() {

		$rotulosTaxonomiaDownloadsDiversos = array(
			'name'              => 'Autor',
			'singular_name'     => 'Categoria de autor',
			'search_items'      => 'Buscar categorias de autor',
			'all_items'         => 'Todas as categorias de autor',
			'parent_item'       => 'Categoria de autor pai',
			'parent_item_colon' => 'Categoria de autor pai:',
			'edit_item'         => 'Editar categoria de autor',
			'update_item'       => 'Atualizar categoria de autor',
			'add_new_item'      => 'Nova categoria de autor',
			'new_item_name'     => 'Nova categoria',
			'menu_name'         => 'Categorias de autor',
		);

		$argsTaxonomiaDownloadsDiversos   = array(
			'hierarchical'      => true,
			'labels'            => $rotulosTaxonomiaDownloadsDiversos,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'autores-downloads-diversos' ),
		);

		register_taxonomy( 'categoriaDownloadsDiversos', array( 'downloads-diversos' ), $argsTaxonomiaDownloadsDiversos );

	}		

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesTatigodoy(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

	function registraMetaboxes( $metaboxes ){

		$prefix = 'Tatigodoy_';

		// METABOX DE DESTAQUE
		$metaboxes[] = array(

			'id'			=> 'detailsMetaboxTeam',
			'title'			=> 'Team member details',
			'pages' 		=> array( 'destaque' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'Link do banner:',
					'id'    => "{$prefix}link_banner_destaque",
					'desc'  => 'Insira aqui o link que o banner terá.',
					'type'  => 'text',
				),
			),
		);

		$metaboxes[] = array(

			'id'			=> 'detalhesDepoimentos',
			'title'			=> 'Detalhes do depoimento',
			'pages' 		=> array( 'depoimentos' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(

				array(
					'name'  => 'ID do vídeo',
					'id'    => "{$prefix}id_video_depoimento",
					'desc'  => 'Insira aqui o ID do vídeo do depoimento. <br> ex: <strong>CuqRveFsOT4</strong>',
					'type'  => 'text',
				),
				array(
					'name'  => 'Bandeira da Cidade',
					'id'    => "{$prefix}bandeira_depoimento",
					'desc'  => 'Insira aqui a bandeira da cidade',
					'type'  => 'image',
					'max_file_uploads' => 1,
				),
			),
		);

		$metaboxes[] = array(

			'id'			=> 'detalhesSobre',
			'title'			=> 'Imagens sobre',
			'pages' 		=> array( 'sobre' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Imagens do carrossel',
					'id'    => "{$prefix}imagens_carrossel_sobre",
					'desc'  => 'Insira aqui as imagens do carrossel sobre a Tati',
					'type'  => 'image_advanced',
				),
			),
		);

		$metaboxes[] = array(
			'id'			=> 'detalhesDestaqueEstanteVirtual',
			'title'			=> 'Destaque Estante Virtual',
			'pages' 		=> array( 'destaque-estante' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Link do banner:',
					'id'    => "{$prefix}link_banner_destaque_estante_virtual",
					'desc'  => 'Insira aqui o link para onde o banner deve redirecionar.',
					'type'  => 'text',
				),
			),
		);

		$metaboxes[] = array(
			'id'			=> 'detalhesEbooksExclusivos',
			'title'			=> 'Detalhes E-book',
			'pages' 		=> array( 'ebooks-exclusivos' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Link do E-book:',
					'id'    => "{$prefix}link_ebook_exclusivo",
					'desc'  => 'Insira aqui o link do ebook.',
					'type'  => 'text',
				),
			),
		);

		$metaboxes[] = array(
			'id'			=> 'detalhesDownloadsDiversos',
			'title'			=> 'Detalhes E-book',
			'pages' 		=> array( 'downloads-diversos' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Link do E-book:',
					'id'    => "{$prefix}link_ebook_diverso",
					'desc'  => 'Insira aqui o link do ebook.',
					'type'  => 'text',
				),
			),
		);

		$metaboxes[] = array(
			'id'			=> 'detalhesVideosRodape',
			'title'			=> 'Detalhes do vídeo',
			'pages' 		=> array( 'videosRodape' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Vídeo:',
					'id'    => "{$prefix}video_rodape_codigo_incorporacao",
					'desc'  => 'Insira aqui o código de incorporação. ex:<strong>iframe width="853" height="480" src="https://www.youtube.com/embed/CuqRveFsOT4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></strong>',
					'type'  => 'text',
				),
			),
		);

		$metaboxes[] = array(
			'id'			=> 'detalhesPost',
			'title'			=> 'Detalhes do post',
			'pages' 		=> array( 'post' ),
			'context' 		=> 'normal',
			'priority' 		=> 'high',
			'autosave' 		=> false,
			'fields' 		=> array(
				array(
					'name'  => 'Minitatura do Post:',
					'id'    => "{$prefix}imagem_post_normal",
					'desc'  => '',
					'type'  => 'image_advanced',
				),
			),
		);

		return $metaboxes;
	}

	function metaboxjs(){

		global $post;
		$template = get_post_meta($post->ID, '_wp_page_template', true);
		$template = explode('/', $template);
		$template = explode('.', $template[1]);
		$template = $template[0];

		if($template != ''){
			wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
		}
	}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesTatigodoy(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerTatigodoy(){

		if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseTatigodoy');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

		baseTatigodoy();

		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );