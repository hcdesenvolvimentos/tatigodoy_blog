<div class="listaDePosts" id="<?php echo $contador2; ?>">
					<div class="row">
						<!-- LISTA DE POSTS ESQUERDA -->
						<div class="col-md-5">
							<div class="postsDestaqueEsquerda">
								<ul>
									<?php 
									//LOOP DE POST CATEGORIA DESTAQUE				
									$postsDestaque = new WP_Query(array(
										'post_type'     => 'post',
										'posts_per_page'   => 2,
										'tax_query'     => array(
											array(
												'taxonomy' => 'category',
												'field'    => 'slug',
												'terms'    => 'destaque',
													)
												)
											)
										);
									while($postsDestaque->have_posts()): $postsDestaque->the_post();
										$categoriaPost = get_the_category();
										$imagemPostDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPostDestaque = $imagemPostDestaque[0];
										global $post;
										foreach ($categoriaPost as $categoriaPost) {
											$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPost->cat_ID);
											$corCategoriaPost = $categoriaPost->description;
										}
									?>
									<!-- POST DESTAQUE -->
									<li>
										<article>
											<a href="<?php echo get_permalink(); ?>">
												<div class="postHover" style="background-color: <?php echo $corCategoriaPost; ?>">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(130); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemPostDestaque" style="background: url(<?php echo $imagemPostDestaque; ?>)">
													<img src="<?php echo $imagemPostDestaque; ?>" alt="">
												</figure>
												<div class="informacoesPostDestaque" style="background-color: <?php echo $corCategoriaPost; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePostDestaque">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPostDestaque">
																<h1 class="tituloPostDestaque"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPostDestaque"><?php echo  get_the_date('j M Y'); ?></h4>
																<p class="descricaoPostDestaque"><?php customExcerpt(130); ?></p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile; wp_reset_query();?>
								</ul>
							</div>
						</div>
						<!-- LISTA DE POSTS DIREITA -->
						<div class="col-md-7">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
									if ( have_posts() ) : 
										while( have_posts() ) : the_post();
											$imagemDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemDestaquePost = $imagemDestaquePost[0];
											global $post;
											$categories = get_the_category();
											foreach ($categories as $categories ) {
												$imagemCategoria = z_taxonomy_image_url($categories->cat_ID);
												$corCategoriaPost = $categories->description;
											}
									?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPost; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoria; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemDestaquePost; ?>)">
													<img src="<?php echo $imagemDestaquePost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPost; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoria; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
																<p class="descricaoPost"><?php customExcerpt(100); ?></p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; endif; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>
					<div class="verMaisPosts">
						<span class="vermais">Ver mais Posts</span>
					</div>
				</div>