<?php
/**
 * The template for displaying category pages
 *
 *
 * @package tatigodoy
 */
$categorias = get_the_category();
$separarBanner = explode("|", get_queried_object()->description);

//VERIFICANDO SE CATEGORIA TEM COR, SENÃO PEGA COR PRADRÃO 
if (!$corCategoria) {
	$corCategoria = "#c7579fba";
}
//VERIFICANDO SE CATEGORIA TEM BANNER, SENÃO PEGA BANNER PRADRÃO
$bannerCategoria = $separarBanner[1];
if (!$bannerCategoria) {
	$bannerCategoria = "http://www.tatigodoy.com.br/wp-content/uploads/2018/10/banner-categoria-2.png";
}
get_header();
?>
	<div class="pg pg-categoria">
		<section class="bannerCategoria" style="background: url(<?php echo $bannerCategoria ?>);">
			<h2 class="nomeCategoria"><?php echo get_queried_object()->name ?></h2>
		</section>
		
		<div class="containerFull">
			<div class="listaDePosts">
				<div class="row">
					<!-- LISTA DE POSTS DIREITA -->
					<div class="col-md-12">
						<div class="listaDePostsDireita">
							<ul>
								<!-- POST DIREITA -->
								<?php 
									while (have_posts()): the_post();
										$imagemPostCategoria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPostCategoria = $imagemPostCategoria[0];
										global $post;
										$iconePost = z_taxonomy_image_url(get_queried_object()->cat_ID);
										
										//VERIFICANDO SE CATEGORIA TEM ÍCONE, SENÃO PEGA ÍCONE PRADRÃO
										if (!$iconePost) {
											$iconePost = "http://tatigodoy.hcdesenvolvimentos.com.br/wp-content/uploads/2018/10/icone3.gif";
										}
										$miniaturaPost = rwmb_meta('Tatigodoy_imagem_post_normal');
										$miniaturaPost = $miniaturaPost[0];

										if($miniaturaPost){
											$imagemPost = $miniaturaPost[0];
										}else{
											$imagemPost = $imagemPostCategoria;
										}
								?>
									<li class="postCategoria">
										<article>
											<a href="<?php echo get_permalink(); ?>">	
												<div class="postHover" style="background-color: <?php echo $corCategoria ?>">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $iconePost ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background-color: <?php echo $corCategoria ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $iconePost ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile; ?>
							</ul>
							<!-- PAGINADOR -->
							<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="newsLetter">
			<h4>Inscreva-se em nossa Newsletter</h4>
			<p>e fique por dentro de todas as novidades!</p>
			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<script type="text/javascript" src="http://localhost/projetos/tatigodoy_blog/wp-includes/js/jquery/jquery.js?ver=2.9"></script>
			<script type="text/javascript" src="http://localhost/projetos/tatigodoy_blog/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.9"></script>
			<script type="text/javascript" src="http://localhost/projetos/tatigodoy_blog/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.9"></script>
			<script type="text/javascript" src="http://localhost/projetos/tatigodoy_blog/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script>
			<script type="text/javascript">
				/* <![CDATA[ */
				var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/tatigodoy_blog/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
				/* ]]> */
			</script><script type="text/javascript" src="http://localhost/projetos/tatigodoy_blog/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script>
			<!--END Scripts-->

			<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5bc8d50e46552-2" class="wysija-msg ajax"></div><form id="form-wysija-html5bc8d50e46552-2" method="post" action="#wysija" class="widget_wysija html_wysija">
					<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Seu e-mail" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
					</span>
				<input class="wysija-submit wysija-submit-field" type="submit" value="Enviar" />
				<input type="hidden" name="form_id" value="2" />
				<input type="hidden" name="action" value="save" />
				<input type="hidden" name="controller" value="subscribers" />
				<input type="hidden" value="1" name="wysija-page" />
				<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
			</form></div>
		</div>
	</div>
<?php get_footer(); ?>