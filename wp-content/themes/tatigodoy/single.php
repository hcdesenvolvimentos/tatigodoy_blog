<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tatigodoy
 */
$categoriaDoPost = get_the_category();	
foreach ($categoriaDoPost as $categoriaDoPost) {
	$id_categoria = $categoriaDoPost->cat_ID;
	$categoriaDoPost = $categoriaDoPost->name;
}
get_header();
?>
		<!-- PG POST -->
		<div class="pg pg-post">
			<div class="containerFull">
				<!-- ONDE VOCE ESTA -->
				<div class="breadcrumbs">
					<ul>
						<li><a href="<?php echo get_home_url(); ?>">Tati Godoy</a></li>
						<li><a href="<?php echo get_category_link($id_categoria) ?>"><?php echo $categoriaDoPost; ?></a></li>
						<li class="ativo"><a href="#"><?php echo get_the_title(); ?></a></li>
					</ul>
				</div>
		
				<div class="row">
					<div class="col-md-9">
						<div class="conteudoPost">
							<div class="tituloPost">
								<h1><?php echo get_the_title(); ?></h1>
								<h3 class="dataPost"><?php the_time('m/j/Y'); ?></h3>
							</div>
							<figure class="bannerPost">
								<?php 
									$imagemDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemDestaque = $imagemDestaque[0];
									global $post;
								?>
								<img src="<?php echo $imagemDestaque ?>" alt="Imagem destaque">
							</figure>

							<?php echo the_content(); ?>

						</div>
					</div>
					<div class="col-md-3">
						<div class="sidebarPost">
							<div class="autorPost">
								<span>Quem Escreveu?</span>
								<figure class="imagemAutor">
									<?php echo get_avatar(get_the_author_meta('ID'), 96); // IMAGEM DO AUTOR ?>
								</figure>
								<h3 class="nomeAutor"><?php the_author_meta('display_name')?></h3>
								<p class="descricaoAutor"><?php echo get_the_author_meta('description') ?></p>
							</div>
						</div>
						<?php get_sidebar(); ?>
						<div class="sidebarPost">
							<div class="postRelacionado">
								<span>Veja também</span>
								<?php 
								$postSidebar = new WP_Query( array( 'post_type' => 'post','orderby' => 'rand','posts_per_page' => 1 )); 
								while($postSidebar->have_posts()) : $postSidebar->the_post();
									$imagemDestaquePostSidebar = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemDestaquePostSidebar = $imagemDestaquePostSidebar[0];
									?>
									<article>
										<a href="<?php echo get_permalink(); ?>">
											<figure>
												<img src="<?php echo $imagemDestaquePostSidebar ?>" alt="<?php echo get_the_title(); ?>">
											</figure>
											<h1 class="tituloPostRelacionado"><?php echo get_the_title(); ?></h1>
										</a>
									</article>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="disqus">
					<?php 
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
						    comments_template();
						endif; 
					?>
				</div>
			</div>
		</div>

<?php

get_footer();
