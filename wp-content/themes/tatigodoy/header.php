<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tatigodoy
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- <link rel="profile" href="https://gmpg.org/xfn/11"> -->
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<!-- TÍTULO -->
	<title>Tati Godoy Blog</title>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" /> 
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121354164-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121354164-6');
</script>


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.1&appId=500021060473162&autoLogAppEvents=1';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- 
	<div class="modalNewsLetter" style="display: none;">
		<div class="conteudoModal">
			<span class="fecharModal"><i class="fas fa-times"></i></span>
			<h5>Inscreva-se em nossa newsletter</h5>
			<p class="descricaoModal">Inscrevendo-se na newsletter você ficará por dentro de todos os novos conteúdos, além de receber em primeira mão todos os novos cursos que estão por vir! Fique por dentro de todas as novidades!</p>
			<div class="formularioNewsletter"> -->


				<!--START Scripts : this is the script part you can add to the header of your theme
				<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-includes/js/jquery/jquery.js?ver=2.9"></script>
				<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.9"></script>
				<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.9"></script>
				<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script>
				<script type="text/javascript">var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"<?php echo get_home_url(); ?>/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};/* ]]> */</script>
				<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.9"></script> -->
				<!--END Scripts

				 <div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5bc74d42e40d0-2" class="wysija-msg ajax"></div><form id="form-wysija-html5bc74d42e40d0-2" method="post" action="#wysija" class="widget_wysija html_wysija">
				<input type="text" name="wysija[user][firstname]" class="wysija-input " title="Nome" placeholder="Seu nome" value="" />
    			<span class="abs-req">
       			<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
    			</span>
				<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Seu email" value="" />
    			<span class="abs-req">
    			<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    			</span>
				<input class="wysija-submit wysija-submit-field" type="submit" value="Enviar" />
			    <input type="hidden" name="form_id" value="2" />
			    <input type="hidden" name="action" value="save" />
			    <input type="hidden" name="controller" value="subscribers" />
			    <input type="hidden" value="1" name="wysija-page" />
   			 	<input type="hidden" name="wysija[user_list][list_ids]" value="3" />
 				</form></div>
			</div>
		</div>
	</div> -->

	<!-- TOPO -->
	<header class="topo">
		<div class="containerFull">
			<div class="row">
				
				<!-- LOGO -->
				<div class="col-sm-2">
					<a href="<?php echo get_home_url(); ?>">
						<img class="img-responsive" src="<?php echo $configuracao['header_logo']['url']; ?>" alt="Logo Tati Godoy">
					</a>
				</div>

				<!-- MENU  -->	
				<div class="col-sm-10">
					<div class="navbar" role="navigation">	
										
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop " data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php 
									$menu = array(
										'theme_location'  => '',
										'menu'            => 'Menu principal',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $menu );
								?>
								<span class="abrirPesquisa">
									<a href="#"><i class="fas fa-search"></i></a>
									
								</span>
							</nav>						
						</div>			
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="pesquisar fechado">
		<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<input type="text" name="s" id="search" placeholder="Digite aqui sua pesquisa">
			<input type="submit" value="Pesquisar">
		</form>
	</div>