<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tatigodoy
 */
global $configuracao;
?>
	<!-- RODAPÉ -->
	<footer class="rodape ">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-4">
					<div class="sobreTatiRodape">
						<h2><?php echo $configuracao['rodape_titulo']; ?></h2>
						<div class="descricaoTati">
							<?php echo $configuracao['rodape_descricao']; ?>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="videoYoutube">
						<span>Ultimo Vídeo</span>
						<button class="carrosselRodapeYtEsquerda"><i class="fas fa-chevron-left"></i></button>
						<button class="carrosselRodapeYtDireita"><i class="fas fa-chevron-right"></i></button>
						<div class="carrosselYoutubeRodape">
							<div id="carrosselYoutubeRodape" class="owl-Carousel">
								
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="pgFacebookRodape">
						<div class="fb-page" data-href="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R">Facilita Organização/Tati Godoy</a></blockquote></div>
					</div>
				</div>
			</div>
			<div class="redesSociais">
				<ul>
					<?php if($configuracao['redes_sociais_facebook']): ?>
					<li><a href="<?php echo $configuracao['redes_sociais_facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a></li>
					<?php endif; if($configuracao['redes_sociais_twitter']): ?>
					<li><a href="<?php echo $configuracao['redes_sociais_twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i> Twitter</a></li>
					<?php endif; if($configuracao['redes_sociais_google_plus']): ?>
					<li><a href="<?php echo $configuracao['redes_sociais_google_plus']; ?>" target="_blank"><i class="fab fa-google-plus"></i> Google +</a></li>
					<?php endif; if($configuracao['redes_sociais_instagram']): ?>
					<li><a href="<?php echo $configuracao['redes_sociais_instagram']; ?>" target="_blank"><i class="fab fa-instagram"></i> Instagram</a></li>
					<?php endif; if($configuracao['redes_sociais_pinterest']):  ?>
					<li><a href="<?php echo $configuracao['redes_sociais_pinterest']; ?>" target="_blank"><i class="fab fa-pinterest"></i> Pinterest</a></li>
					<?php endif; if($configuracao['redes_sociais_youtube']): ?>
					<li><a href="<?php echo $configuracao['redes_sociais_youtube']; ?>" target="_blank"><i class="fab fa-youtube"></i> Youtube</a></li>
					<?php endif; if($configuracao['redes_sociais_email']): ?>
					<li><a href="mailto:<?php echo $configuracao['redes_sociais_email']; ?>" ><i class="far fa-envelope"></i> E-mail</a></li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="copyright">
				<p>Todos os direitos reservados</p>
			</div>
			<div class="developerBy">
				<a href="https://hcdesenvolvimentos.com.br" target="_blank">
					<span>Desenvolvido por</span>
					<figure>
					<img src="<?php echo get_template_directory_uri() ?>/img/hc.png" alt="HC Desenvolvimentos">
					</figure>
				</a>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>
</body>
</html>
