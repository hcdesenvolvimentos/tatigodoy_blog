<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package tatigodoy
 */

get_header();
?>
<div class="pg pg-404" style="display: ;">
	<a href="<?php echo get_home_url(); ?>" class="voltarHome">Continue navegando :)</a>
	<img src="<?php echo get_template_directory_uri(); ?>/img/404.png" alt="404">
</div>
