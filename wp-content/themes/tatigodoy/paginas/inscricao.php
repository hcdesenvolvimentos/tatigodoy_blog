<?php
/**
 * Template Name: Inscrição
 *
 * @package tatigodoy
 */

get_header();
?>

<div class="pg pg-inscricao" style="display: ;">
			<section class="bannerInscricao" style="background: url(<?php echo $configuracao['pg_inscricao_banner_img']['url'] ?>)">
				<h2 class="titulo"><?php echo $configuracao['pg_inscricao_banner_titulo']; ?></h2>
			</section>

			<section class="descricaoCurso">
				<h2 class="titulo"><?php echo $configuracao['pg_inscricao_conteudo_titulo']; ?></h2>
				<h3 class="subtitulo"><?php echo $configuracao['pg_inscricao_conteudo_subtitulo']; ?></h3>
				<?php echo $configuracao['pg_inscricao_conteudo_conteudo']; ?>
				
				<div class="formularioInscricao">
					<div class="row">
						<?php echo do_shortcode('[contact-form-7 id="6086" title="Inscrições"]'); ?>
					</div>
						
						
				</div>
			</section>
		</div>
<?php get_footer(); ?>