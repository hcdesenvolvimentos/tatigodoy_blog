<?php
/**
 * Template Name: Sobre
 *
 * @package tatigodoy
 */
get_header();
?>
<div class="pg pg-sobre">
		<div class="containerFull">
			<div class="row">
				<div class="col-md-9">
					<section class="carrosselSobre">
						<h6 class="hidden">Carrossel Sobre</h6>
						<div id="carrosselSobreTati" class="owl-Carousel carrosselSobreTati">
							<!-- ITEM -->
							<?php 
								$galeriaSobre = explode(',', $configuracao['opt_sobre_galeria']);

								foreach ($galeriaSobre as $galeriaSobre):
									$imagem = wp_get_attachment_url($galeriaSobre);

							?>
							<figure class="item">
								<a href="">
									<img class="img-responsive" src="<?php echo $imagem; ?>" alt="Tati Godoy">
								</a>
							</figure>
							<?php endforeach; ?>
						</div>
					</section>
					
					<section class="conteudoSobre">
						<h2 class="titulo"><?php echo get_the_title(); ?></h2>

						<?php echo the_content(); ?>
					</section>


					<div class="entreEmContato">
						<p>Quer entrar em contato? <a href="<?php echo get_home_url(); ?>/contato">Clique aqui!</a></p>
					</div>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
					<div class="sidebarPost">
						<div class="postRelacionado">
							<span>Veja também</span>
							<?php 
							$postSidebar = new WP_Query( array( 'post_type' => 'post','orderby' => 'rand','posts_per_page' => 1 )); 
							while($postSidebar->have_posts()) : $postSidebar->the_post();
								$imagemDestaquePostSidebar = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$imagemDestaquePostSidebar = $imagemDestaquePostSidebar[0];
								?>
								<article>
									<a href="<?php echo get_permalink(); ?>">
										<figure>
											<img src="<?php echo $imagemDestaquePostSidebar ?>" alt="<?php echo get_the_title(); ?>">
										</figure>
										<h1 class="tituloPostRelacionado"><?php echo get_the_title(); ?></h1>
									</a>
								</article>
							<?php endwhile; wp_reset_query(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>