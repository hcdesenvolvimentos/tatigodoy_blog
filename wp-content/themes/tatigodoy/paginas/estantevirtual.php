<?php
/**
 * Template Name: Estante Virtual
 * @package tatigodoy
 */

get_header();
?>

<div class="pg pg-estanteVirtual">
	<div class="containerFull">
		<?php $destaqueEstanteVirtual = new WP_Query(array('post_type' => 'destaque-estante', 'posts_per_page' => -1)); 
		
		if($destaqueEstanteVirtual->have_posts()):
		?>
		<section class="estanteVirtual">
			<h6 class="hidden">Estante virtual</h6>
			<button class="esquerdaCarrosselEstante"><i class="fas fa-angle-left"></i></button>
			<button class="direitaCarrosselEstante"><i class="fas fa-angle-right"></i></button>
			
			<div class="owl-Carrossel" id="carrosselEstante">
				<?php  
				
					while($destaqueEstanteVirtual->have_posts()):
					$destaqueEstanteVirtual->the_post();
					$bannerDestaqueEstanteVirtual = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$bannerDestaqueEstanteVirtual = $bannerDestaqueEstanteVirtual[0];
					
					$linkDestaqueEstanteVirtual = rwmb_meta('Tatigodoy_link_banner_destaque_estante_virtual');
				?>
				<!-- ITEM -->
				<figure class="item">
					<a href="<?php echo $linkDestaqueEstanteVirtual; ?>">
						<img class="img-responsive" src="<?php echo $bannerDestaqueEstanteVirtual ?>" alt="<?php echo get_the_title(); ?>">
					</a>
				</figure>

				<?php endwhile;  wp_reset_query(); ?>
			</div>
		</section>
		<?php endif; ?>
		

		<?php $ebooksExclusivos = new WP_Query( array('post_type' => 'ebooks-exclusivos', 'posts_per_page' => -1));  
		if($ebooksExclusivos->have_posts()):
		?>
		<section class="ebooksGratis">
			<h1>Baixe agora mesmo gratuitamente e-books exclusivos!</h1>
			<button class="esquerdaCarrosselEbooks"><i class="fas fa-angle-left"></i></button>
			<button class="direitaCarrosselEbooks"><i class="fas fa-angle-right"></i></button>
			<div class="carrosselEbooks owl-Carousel" id="carrosselEbooks">
			<?php 
			  	if($ebooksExclusivos->have_posts()):
			  		while ($ebooksExclusivos->have_posts()): 
				  		$ebooksExclusivos->the_post();

				  		$imagemEbookExclusivo = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$imagemEbookExclusivo = $imagemEbookExclusivo[0];
						global $post;

						$linkEbookExclusivo = rwmb_meta('Tatigodoy_link_ebook_exclusivo');

						$listaAutor = get_the_terms($post->ID, 'categoriaEbooksExclusivos'); // LISTANDO A TAXONOMIA PRESENTE NO POST

						foreach ($listaAutor as $listaAutor) {
							$autor = $listaAutor->name; // PERCORRENDO A TAXONOMIA PARA PEGAR O NOME
						}
			?>
				<!-- ITEM -->
				<figure class="item">
					<a href="<?php echo $linkEbookExclusivo; ?>">
						<img class="img-responsive" src="<?php echo $imagemEbookExclusivo; ?>" alt="<?php echo get_the_title(); ?>">
						<h3 class="tituloEbook"><?php echo get_the_title(); ?></h3>
						<h4 class="autorEbook"><?php echo $autor; ?></h4>
					</a>
				</figure>
			<?php endwhile; wp_reset_query(); endif; ?>
			</div>
		</section>
		<?php endif; ?>
	
		<?php $downloadsDiversos = new WP_Query( array('post_type' => 'downloads-diversos', 'posts_per_page' => -1));  
		if($downloadsDiversos->have_posts()):
		?>
		<section class="downloadsDiversos">
			<h3>Downloads diversos</h3>
			<button class="esquerdaCarrosselDownloads"><i class="fas fa-angle-left"></i></button>
			<button class="direitaCarrosselDownloads"><i class="fas fa-angle-right"></i></button>
			<div id="carrosselDownloadsDiversos" class="owl-Carousel">
				<?php  
					while ($downloadsDiversos->have_posts()):
						$downloadsDiversos->the_post();

						$imagemDownloadDiverso = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$imagemDownloadDiverso = $imagemDownloadDiverso[0];

						$linkDownloadDiverso = rwmb_meta('Tatigodoy_link_ebook_diverso');

						$listaAutor = get_the_terms($post->ID, 'categoriaDownloadsDiversos'); // LISTANDO A TAXONOMIA PRESENTE NO POST

						foreach ($listaAutor as $listaAutor) {
							$autor = $listaAutor->name; // PERCORRENDO A TAXONOMIA PARA PEGAR O NOME
						}

				?>
				<!-- ITEM -->
				<figure class="item">
					<a href="<?php echo $linkDownloadDiverso; ?>">
						<img class="img-responsive" src="<?php echo $imagemDownloadDiverso; ?>" alt="<?php echo get_the_title(); ?>">
						<h3 class="tituloDownload"><?php echo get_the_title(); ?></h3>
						<h4 class="autorDownload"><?php echo $autor; ?></h4>
					</a>
				</figure>

				<?php endwhile; wp_reset_query(); ?>

			</div>
		</section>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>