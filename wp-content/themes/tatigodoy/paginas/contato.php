<?php
/**
 * Template Name: Contato
 * @package tatigodoy
 */

get_header();
?>
<div class="pg pg-contato">
	<div class="bannerContato" style="background: url(<?php echo $configuracao['pg_contato_banner_img']['url'] ?>)"></div>
	<section class="descricaoContato">
		<div class="containerFull">
			<h5 class="titulo"><?php echo $configuracao['pg_contato_conteudo_titulo']; ?></h5>
			<p><?php echo $configuracao['pg_contato_conteudo_subtitulo']; ?></p>

			<section class="redesSociaisContato">
				<div class="row">
					<div class="col-sm-6">
						<div class="redesSociais">
							<ul>
								<?php if($configuracao['redes_sociais_email']): ?>
								<li>
									<a href="mailto:<?php echo $configuracao['redes_sociais_email']; ?>">
										<span class="iconeRedeSocial"><i class="far fa-envelope"></i></span>
										<p>E-mail</p>
										<small class="linkContato"><?php echo $configuracao['redes_sociais_email']; ?></small>
									</a>
								</li>
								<?php endif; 
								if($configuracao['redes_sociais_telefone']):
								?>
								<li>
									<a href="tel:<?php echo $configuracao['redes_sociais_telefone']; ?>">
										<span class="iconeRedeSocial"><i class="fas fa-phone"></i></span>
										<p>Telefone</p>
										<small class="linkContato"><?php echo $configuracao['redes_sociais_telefone']; ?></small>
									</a>
								</li>
								<?php endif; 
								if($configuracao['redes_sociais_instagram']):
								?>
								<li>
									<a href="<?php echo $configuracao['redes_sociais_instagram'] ?>">
										<span class="iconeRedeSocial"><i class="fab fa-instagram"></i></span>
										<p>instagram</p>
										<small class="linkContato">@tatigodoyfacilita</small>
									</a>
								</li>
								<?php endif;  
									if($configuracao['redes_sociais_facebook']):
								?>
								<li>
									<a href="<?php echo $configuracao['redes_sociais_facebook']; ?>">
										<span class="iconeRedeSocial"><i class="fab fa-facebook-f"></i></span>
										<p>facebook</p>
										<small class="linkContato">/FacilitaOrganizacao</small>
									</a>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="formularioContato">
							<p class="tituloFormulario"><?php echo $configuracao['pg_contato_formulario_titulo']; ?></p>
							<span class="garantiaPrivacidade"><?php echo $configuracao['pg_contato_formulario_subtitulo'] ?></span>

							<div class="formulario">
								<?php echo do_shortcode('[contact-form-7 id="114" title="Contato"]'); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
</div>


<?php get_footer(); ?>