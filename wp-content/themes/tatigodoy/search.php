<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package tatigodoy
 */


/* translators: %s: search query. */
//printf( esc_html__( 'Search Results for: %s', 'tatigodoy' ), '<span>' . get_search_query() . '</span>' );


get_header();
?>
	<div class="pg pg-busca">
		<div class="containerFull">
			<!-- ONDE VOCE ESTA -->
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo get_home_url(); ?>">Tati Godoy</a></li>
					<li><a href="  ">Pesquisa</a></li>
					<li class="ativo"><a href=" "><?php echo get_search_query(); ?></a></li>
				</ul>
			</div>
	

		<?php if ( have_posts() ) : ?>
				<section class="resultadosBusca">
					<h3><?php printf( esc_html__( 'Resultados encontrados para: %s', 'tatigodoy' ), '"' . get_search_query() . '"' ); ?></h3>
					<div class="postsEncontrados">
						<ul>
							<?php  
								while (have_posts()): the_post();
									$categoriaPost = get_the_category();
									$imagemPostDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
									$imagemPostDestaque = $imagemPostDestaque[0];
									global $post;
									foreach ($categoriaPost as $categoriaPost) {
										$iconeCategoriaPost = z_taxonomy_image_url($categoriaPost->cat_ID);
										$separarBanner = explode("|", $categoriaPost->description);
										$corCategoriaPost = $separarBanner[0];
									}
									$miniaturaPost = rwmb_meta('Tatigodoy_imagem_post_normal');
									
									if($miniaturaPost){
										$imagemPost = $miniaturaPost[0];
									}else{
										$imagemPost = $imagemPostDestaque;
									}
							?>
							<!-- POST DIREITA -->
							<li>
								<article>
									<a href="<?php echo get_permalink(); ?>">	
										<div class="postHover" style="background:<?php echo $corCategoriaPost ?>">
											<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
											<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
											<p class="descricaoPostHover"><?php customExcerpt(130); ?></p>
											<img src="img/iconePostNormal.png" alt="">
										</div>	
										<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
											<img src="<?php echo $imagemPost ?>" alt="<?php echo get_the_title(); ?>">
										</figure>
										<div class="informacoesPost" style="background: <?php echo $corCategoriaPost ?>">
											<div class="row">
												<div class="col-xs-3">
													<div class="iconePost">
														<img src="<?php echo $iconeCategoriaPost ?>" alt="<?php echo get_the_title(); ?>">
													</div>
												</div>
												<div class="col-xs-9">
													<div class="detalhesPost">
														<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
														<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
														<p class="descricaoPost"><?php customExcerpt(130); ?></p>
													</div>
												</div>
											</div>
										</div>
									</a>
								</article>
							</li>
						<?php endwhile; wp_reset_query(); ?>
						</ul>
						<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
					</div>
				</section>
			

		<?php else: ?>
			<div class="notFoundSearch">
				<div class="content_center">
					<h3>Não encontramos sua busca por: "<?php echo get_search_query(); ?>"</h3>
				<a href="<?php echo get_home_url(); ?>">Continue navegando</a>
				</div>
			</div>
		<?php endif; ?>
		
		</div>
	</div>
		
<?php
get_footer();
