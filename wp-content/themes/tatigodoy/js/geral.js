$(function(){
	/*****************************************
	*           CARROSSEIS                   *
	*****************************************/
		//CARROSSEL DE DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
	        dots: true,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    //responsiveClass:true,			    
	        /*responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:2
	            },
	            1024:{
	                items:3
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }*/		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL ESTANTE
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('.carrosselDestaqueEsquerda').click(function(){ carrossel_destaque.prev(); });
		$('.carrosselDestaqueDireita').click(function(){ carrossel_destaque.next(); });

		//CARROSSEL SOBRE TATI
		$("#carrosselSobreTati").owlCarousel({
			items : 5,
	        dots: false,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag:false,
	        touchDrag  : false,
	        //autoplay:true,
		    //autoplayTimeout:1500,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    center: true,
		    autoplay:true,
		     autoplayTimeout:31,
		     smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:3
	            },
	            600:{
	                items:3
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:5
	            },
	            1440:{
	                items:5
	            },
	            			            
	        }		    		   		    
		    
		});
		
		//CARROSSEL PRINCIPAL ESTANTE VIRTUAL	
		$("#carrosselEstante").owlCarousel({
			items : 1,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    /*responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:2
	            },
	           
	            991:{
	                items:2
	            },
	            1024:{
	                items:3
	            },
	            1440:{
	                items:4
	            },
	            			            
	        }	*/	    		   		    
		    
		});
		//BOTÕES DO CARROSSEL ESTANTE
		var carrossel_estante = $("#carrosselEstante").data('owlCarousel');
		$('.esquerdaCarrosselEstante').click(function(){ carrossel_estante.prev(); });
		$('.direitaCarrosselEstante').click(function(){ carrossel_estante.next(); });

		//CARROSSEL EBOOKS EXCLUSIVOS
		$("#carrosselEbooks").owlCarousel({
			items : 7,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:2
	            },
	            600:{
	                items:4
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:7
	            },
	            			            
	        }		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL EBOOKS EXCLUSIVOS
		var carrossel_ebooks = $("#carrosselEbooks").data('owlCarousel');
		$('.esquerdaCarrosselEbooks').click(function(){ carrossel_ebooks.prev(); });
		$('.direitaCarrosselEbooks').click(function(){ carrossel_ebooks.next(); });

		//CARROSSEL DOWNLOADS DIVERSOS
		$("#carrosselDownloadsDiversos").owlCarousel({
			items : 6,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:false,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            445:{
	                items:2
	            },
	            600:{
	                items:3
	            },
	           
	            991:{
	                items:5
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:6
	            },
	        }		    		   		    
		    
		});
		//BOTÕES DO CARROSSEL EBOOKS EXCLUSIVOS
		var carrossel_downloads_diversos = $("#carrosselDownloadsDiversos").data('owlCarousel');
		$('.esquerdaCarrosselDownloads').click(function(){ carrossel_downloads_diversos.prev(); });
		$('.direitaCarrosselDownloads').click(function(){ carrossel_downloads_diversos.next(); });
			
		//CARROSSEL CATEGORIAS
		$("#carrosselCategorias").owlCarousel({
			items :4,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    //smartSpeed: 450,
		    // center: true,
		    // autoplay:true,
		    //  autoplayTimeout:31,
		    //  smartSpeed:7500,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1
	            },
	            600:{
	                items:3
	            },
	           
	            991:{
	                items:4
	            },
	            1024:{
	                items:4
	            },
	            1440:{
	                items:4
	            },
	        }		    		   		    
		    
		});	

		$(window).load(function(){
			setTimeout(function(){ 
				//CARROSSEL DE VÍDEO RODAPÉ
			$("#carrosselYoutubeRodape").owlCarousel({
				items : 1,
		        dots: false,
		        loop: false,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    smartSpeed: 450,

			});
			//BOTÕES DO CARROSSEL ESTANTE
			var carrossel_yt_rodape = $("#carrosselYoutubeRodape").data('owlCarousel');
			$('.carrosselRodapeYtEsquerda').click(function(){ carrossel_yt_rodape.prev(); });
			$('.carrosselRodapeYtDireita').click(function(){ carrossel_yt_rodape.next(); });
			}, 100);
			
		})
			
	/*****************************************
					SCRIPTS GERAIS
	******************************************/

		$(window).bind('scroll', function () {
	       
	       var alturaScroll = $(window).scrollTop()
	       if(alturaScroll > 300){
	       	  $(".topo").addClass('thin');
	       }
	       else{
	       	  $(".topo").removeClass('thin');
	       }
	    });

	    $(".pg-inicial .postsPaginaInicial .categoriasPosts ul li button").click(function(){
	    	$(this).next().removeClass("abrirTab");
	    	$(this).next().addClass("abrirTab");
	    });

	 // 	$( window ).load(function() {
	 // 	setTimeout(function(){ $(".loading").fadeOut()}, 1000);
		// });

		$(document).ready(function(){
			setTimeout(function(){ 
				$(".loading").addClass("green");
				$(".loading .centerAll span.aguarde").addClass('fadeOut');
				$(".loading .centerAll span.carregandoSite").removeClass('fadeOut');
			}, 2000);
			
		})

		$(window).load(function() {

			setTimeout(function(){ 
				$(".loading .centerAll span.carregandoSite").addClass('fadeOut');
				$(".loading .centerAll span.wellcome").removeClass('fadeOut');
				$(".loading").removeClass("green");
				$(".loading").addClass("pink");
			}, 1000);
			setTimeout(function(){ 
				$(".loading").fadeOut();
			}, 2000);
		})


	$('.pg .listaDePosts.tab_cont').fadeOut();
	$(".pg-inicial .postsPaginaInicial .categoriasPosts ul li .linkCategoriaPai").click(function(e) {
		e.preventDefault();
		let id_categoria = $(this).attr("data-id-tab");
		$(this).children().addClass('ativo');
		$('.pg .listaDePosts').fadeOut();
		$('.pg .listaDePosts#'+ id_categoria).fadeIn();
	});


	$('.pg-inicial .postsPaginaInicial .abrirMenuCategorias').click(function(){
		$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile').addClass('abrir');
	});

	$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile .fechar').click(function(){
		$('.pg-inicial .postsPaginaInicial .menuCategoriasMobile').removeClass('abrir');
	});

		$(window).load(function() {
			setTimeout(function(){
				$(".modalNewsLetter").fadeIn();
			},7000)
		});


		$(".modalNewsLetter .conteudoModal .fecharModal").click(function(event) {
			$(".modalNewsLetter").fadeOut();
		});

		// SCRIPT PARA DETECTAR O 'esc' DO TECLADO PARA FECHAR A MODAL
	 	$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$(".modalNewsLetter").fadeOut();
			}
		});

	 	$(document).ready(function($) {
	 		var htmlString = "";
	        var apiKey = 'AIzaSyBEP5unPBzF7n3J-ZtnVnip3IqcQ5KlSWc'; //CHAVE API https://console.developers.google.com
	        var channelID = 'UCBlBXUL1EuSFQX_u1LJ_yNw'; //ID DO CANAL
	        var maxResults = 2;

	        $.getJSON('https://www.googleapis.com/youtube/v3/search?key=' + apiKey + '&channelId=' + channelID + '&part=snippet,id&order=date&maxResults=' + (maxResults > 2 ? 2 : maxResults), function(data) {
	        	$.each(data.items, function(i, item) {
	        		var videoID = item['id']['videoId'];
	        		var title = item['snippet']['title'];
	        		var videoEmbed = "https://www.youtube.com/embed/" + videoID;
	        		htmlString += '<div class="item"><div class="iframeYoutube"><iframe width="100%" height="400" src="' + videoEmbed + '" allow="autoplay; encrypted-media" frameborder="0" allowfullscreen></iframe></div></div>';
	        	});
	        	$('#carrosselYoutubeRodape').html(htmlString);
	        });	 	
	    });
	 	
 		$(".topo .navbar .navbar-header .navbar-collapse .abrirPesquisa").click(function(event) {
 			var larguraTela = window.innerWidth;
 			if(larguraTela <=767){
 				$(".topo .navbar .navbar-header .navbar-collapse").removeClass('in');
 				$(".pesquisar").toggleClass('fechado');
 				$('.topo .navbar button').removeClass('exchangeButton');
 			}else{
				$(".pesquisar").toggleClass('fechado');
 			}
		});
	 	
	 	$('.topo .navbar button').click(function() {
	 		let verif = $(this).attr('aria-expanded');
	 		if(verif == "true" || verif == "undefined"){
	 			$(this).removeClass('exchangeButton');
	 		}else{
	 			$(this).addClass('exchangeButton');
	 		}
	 		
	 		setTimeout(function(){
	 			if(verif == "true" || verif == "undefined"){
	 				$(this).removeClass('exchangeButton');
	 			}else{
	 				$(this).addClass('exchangeButton');
	 			}
	 		}, 100);
	 	});




// $('.modalNewsLetter').on('hide', function (e) {
// 	var checkModal = false;
// 	writeCookie('modal', checkModal, 1);
// });


});