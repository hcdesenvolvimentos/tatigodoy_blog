<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package tatigodoy
 */
global $post;

get_header();
?>
	<div class="pg pg-depoimentos" style="display:;">
		<section class="bannerDepoimentos">
			<video class="fundoDepoimentos" autoplay muted>
				<!--<source src="<?php //echo get_template_directory_uri(); ?>/videos/video.mp4" type="video/mp4"></source>-->
				<source src="<?php echo $configuracao['pg_depoimentos_banner_video']['url'] ?>" type="video/mp4"></source>
			</video>
			<h2><?php echo $configuracao['pg_depoimentos_banner_titulo']; ?></h2>
		</section>
		<section class="depoimentos">
		

			<div class="containerFull">
				<h3><?php echo $configuracao['pg_depoimentos_conteudo_titulo']; ?></h3>
				<h6><?php echo $configuracao['pg_depoimentos_conteudo_subtitulo'] ?></h6>
				
				<?php 
					$contador = 0;
					while(have_posts()): 
						the_post();
						$id_video = rwmb_meta('Tatigodoy_id_video_depoimento');
						$estado = rwmb_meta('Tatigodoy_bandeira_depoimento');

						foreach($estado as $estado){
							$estado = $estado['full_url'];
						}

					
					if($contador % 2 == 0):
				?>
					<!-- TEXTO ESQUERDA VIDEO DIREITA -->
					<div class="row">
						<div class="col-sm-5">
							<div class="textoDepoimentos">
								<h1 class="tituloDepoimento">
									<?php echo get_the_title();?>
									<?php  if($estado):  ?>
										<img src="<?php echo $estado; ?>" alt="<?php echo get_the_title(); ?>">
									<?php endif; ?>
								</h1>
								<?php echo the_content(); ?>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="videoYoutube">
								<iframe width="876" height="373" src="https://www.youtube.com/embed/<?php echo $id_video ?>?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					<div class="bordaInferiorDepoimentos"></div>
					<?php else: ?>
					<!-- VIDEO ESQUERDA TEXTO DIREITA-->
					<div class="row">
						<div class="col-sm-7">
							<div class="videoYoutube">
								<iframe width="876" height="373" src="https://www.youtube.com/embed/<?php echo $id_video ?>?ecver=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="textoDepoimentos">
								<h1 class="tituloDepoimento">
									<?php echo get_the_title(); ?>
									<?php  if($estado):  ?>
										<img src="<?php echo $estado; ?>" alt="<?php echo get_the_title(); ?>">
									<?php endif; ?>
								</h1>
								<?php echo the_content(); ?>
							</div>
						</div>
					</div>
					<div class="bordaInferiorDepoimentos"></div>

				<?php endif;$contador++; endwhile;  ?>
				<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
			</div>
		</section>
	</div>

<?php get_Footer(); ?>