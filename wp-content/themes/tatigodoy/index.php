<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tatigodoy
 */

get_header();
?>
	<div class="loading">
 		<div class="centerAll">
			<img src="<?php echo get_template_directory_uri() ?>/img/logotati.png" alt="Carregando">
 			<span class="aguarde">Aguarde um momento</span>
 			<span class="carregandoSite fadeOut">Estamos carregando o site</span>
 			<span class="wellcome fadeOut">Seja bem vindo</span>
 		</div>
	</div> 
	<!-- PG INICIAL -->
	<div class="pg pg-inicial">
		<!-- CARROSSEL DE DESTAQUE -->
		<section class="carrosselDestaque">
			<?php $bannerDestaque = new WP_Query( array( 'post_type' => 'destaque', 'posts_per_page' => -1 )); ?>
			<button class="carrosselDestaqueEsquerda"></button>
			<button class="carrosselDestaqueDireita"></button>
			<h6 class="hidden">Carrossel Destaque</h6>
			<div id="carrosselDestaque" class="owl-Carousel">
				<?php
					while($bannerDestaque->have_posts()): $bannerDestaque->the_post();
					$imagemBannerDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$imagemBannerDestaque = $imagemBannerDestaque[0];
					global $post;
					$linkBannerDestaque = rwmb_meta('Tatigodoy_link_banner_destaque');
				?>	
				<!-- ITEM -->
				<figure class="item">
					<a href="<?php echo $linkBannerDestaque; ?>" title="<?php echo get_the_title(); ?>">
						<img class="img-responsive" src="<?php echo $imagemBannerDestaque; ?>" alt="<?php echo get_the_title(); ?>">
					</a>
				</figure>
				<?php endwhile; wp_reset_query();?>
			</div>
		</section>

		<section class="postsPaginaInicial">
			<div class="containerFull">	

				<div class="abrirMenuCategorias">VER CATEGORIAS</div>
					
				<div class="menuCategoriasMobile">
					<span class="fechar">x</span>
					<h3 class="titulo">Categorias</h3>
					<ul>
						<li>
							<a href="#" class="linkCategoriaPai">
								<h3 class="">Organização é Vida</h3>
							</a>
							<ul class="submenu">
								
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizacao-pessoal/" title=""> <h3 class="submenu-cont" >Organização Pessoal</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/rotina-domestica-organizacao/" title=""> <h3 class="submenu-cont" >Rotina Doméstica</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-a-mente/" title=""> <h3 class="submenu-cont" >Organizando a Mente</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-os-ambientes/" title=""> <h3 class="submenu-cont" >Organizando os Ambientes</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-seu-tempo/" title=""> <h3 class="submenu-cont" >Organizando seu Tempo</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-a-vida-financeira/" title=""> <h3 class="submenu-cont" >Organizando a Vida Financeira</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/personal-organizer-organizacao/" title=""> <h3 class="submenu-cont" >Personal Organizer</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai">
								<h3 class="" >Limpeza Fácil</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/limpezas/limpeza-pos-obra/" title=""><h3 class="submenu-cont" >Limpeza Pós-Obra</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/limpezas/limpeza-residencial/" title=""><h3 class="submenu-cont" >Limpeza Residencial</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai">
								<h3 class="" >Curso de Personal Organizer</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/cursos/agenda/" title=""><h3 class="submenu-cont" >Agenda de Cursos</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/cursos/cursos-realizados/" title=""><h3 class="submenu-cont" >Cursos já realizados</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai">
								<h3 class="" >Bem Estar</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>/bem-estar/vida-saudavel/" title=""><h3 class="submenu-cont" >Vida Saudável</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>/bem-estar/equilibrio-espiritual/" title=""><h3 class="submenu-cont" >Equilíbrio Espiritual</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>bem-estar/bem-estar-em-familia/" title=""><h3 class="submenu-cont" >Bem-estar em Família</h3></a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<?php $contador = 0; ?>
				<div class="categoriasPosts">
					<ul>
						<li>
							<a href="#" class="linkCategoriaPai" data-id-tab="1">
								<h3 class="">Organização é Vida</h3>
							</a>
							<ul class="submenu">
								
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizacao-pessoal/" title=""> <h3 class="submenu-cont">Organização Pessoal</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/rotina-domestica-organizacao/" title=""> <h3 class="submenu-cont" >Rotina Doméstica</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-a-mente/" title=""> <h3 class="submenu-cont" >Organizando a Mente</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-os-ambientes/" title=""> <h3 class="submenu-cont" >Organizando os Ambientes</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-seu-tempo/" title=""> <h3 class="submenu-cont" >Organizando seu Tempo</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/organizando-a-vida-financeira/" title=""> <h3 class="submenu-cont" >Organizando a Vida Financeira</h3></a>
								</li>
								<li class="submenu-list">

									<a href="<?php echo get_home_url(); ?>/organizacoes/personal-organizer-organizacao/" title=""> <h3 class="submenu-cont" >Personal Organizer</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai" data-id-tab="2">
								<h3 class="" >Limpeza Fácil</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/limpezas/limpeza-pos-obra/" title=""><h3 class="submenu-cont" >Limpeza Pós-Obra</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/limpezas/limpeza-residencial/" title=""><h3 class="submenu-cont" >Limpeza Residencial</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai" data-id-tab="3">
								<h3 class="" >Curso de Personal Organizer</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/cursos/agenda/" title=""><h3 class="submenu-cont" >Agenda de Cursos</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo get_home_url(); ?>/cursos/cursos/" title=""><h3 class="submenu-cont" >Cursos já realizados</h3></a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" class="linkCategoriaPai" data-id-tab="4">
								<h3 class="" >Bem Estar</h3>
							</a>
							<ul class="submenu">
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>/bem-estar/vida-saudavel/" title=""><h3 class="submenu-cont" >Vida Saudável</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>/bem-estar/equilibrio-espiritual/" title=""><h3 class="submenu-cont" >Equilíbrio Espiritual</h3></a>
								</li>
								<li class="submenu-list">
									<a href="<?php echo home_url(); ?>/bem-estar/bem-estar-em-familia/" title=""><h3 class="submenu-cont" >Bem-estar em Família</h3></a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				<?php $slugsCategoria = array(
					0 => 'organizacoes',
					1 => 'limpezas',
					2 => 'personal-organizer-organizacao',
					3 => 'bem-estar',
					); 
				?>
				<?php $contador2 = 0; ?>
				<div class="listaDePosts" id="<?php echo $contador2; ?>">
					<div class="row">
						<!-- LISTA DE POSTS ESQUERDA -->
						<div class="col-md-5">
							<div class="postsDestaqueEsquerda">
								<ul>
									<?php 
									//LOOP DE POST CATEGORIA DESTAQUE				
									$postsDestaque = new WP_Query(array(
										'post_type'     => 'post',
										'posts_per_page'   => 2,
										'tax_query'     => array(
											array(
												'taxonomy' => 'category',
												'field'    => 'slug',
												'terms'    => 'destaque',
													)
												)
											)
										);
									while($postsDestaque->have_posts()): $postsDestaque->the_post();
										$categoriaPost = get_the_category();
										$imagemPostDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$imagemPostDestaque = $imagemPostDestaque[0];
										global $post;
										foreach ($categoriaPost as $categoriaPost) {
											$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPost->cat_ID);
											$separarBanner = explode("|", $categoriaPost->description);
											$corCategoriaPost = $separarBanner[0];
										}
										$miniaturaPost = rwmb_meta('Tatigodoy_imagem_post_normal');
										$miniaturaPost = $miniaturaPost[0];
										if($miniaturaPost){
											$imagemPost = $miniaturaPost[0];
										}else{
											$imagemPost = $imagemPostDestaque;
										}
									?>
									<!-- POST DESTAQUE -->
									<li>
										<article>
											<a href="<?php echo get_permalink(); ?>">
												<div class="postHover" style="background-color: <?php echo $corCategoriaPost; ?>">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(130); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemPostDestaque" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="">
												</figure>
												<div class="informacoesPostDestaque" style="background-color: <?php echo $corCategoriaPost; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePostDestaque">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPostDestaque">
																<h1 class="tituloPostDestaque"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPostDestaque"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile; wp_reset_query();?>
								</ul>
							</div>
						</div>
						<!-- LISTA DE POSTS DIREITA -->
						<div class="col-md-7">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
									if ( have_posts() ) : 
										while( have_posts() ) : the_post();
											$imagemDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemDestaquePost = $imagemDestaquePost[0];
											global $post;
											$categories = get_the_category();
											foreach ($categories as $categories ) {
												$imagemCategoria = z_taxonomy_image_url($categories->cat_ID);
												$separarBanner = explode("|", $categories->description);
												$corCategoriaPostNormal = $separarBanner[0];
											}
											if($miniaturaPost){
												$imagemPost = $miniaturaPost[0];
											}else{
												$imagemPost = $imagemDestaquePost;
											}

									?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPostNormal; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoria; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPostNormal; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoria; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; endif; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>

					<!-- DIV PAGINADOR -->
					<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
				</div>

				<div class="listaDePosts tab_cont" id="1">
					<div class="row">
						<?php 
						//LOOP DE POST POR CATEGORIA		
						$postTabs = new WP_Query(array(
							'post_type'     => 'post',
							'posts_per_page'   => 10,
							'tax_query'     => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $slugsCategoria[0],
										)
									)
								)
							);
						?>
						<div class="col-md-12">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
										while($postTabs->have_posts()): $postTabs->the_post();
											$categoriaPostTabs = get_the_category();
											$imagemPostTabs = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemPostTabs = $imagemPostTabs[0];
											global $post;
											foreach ($categoriaPostTabs as $categoriaPostTabs) {
												$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPostTabs->cat_ID);
												$separarBanner = explode("|", $categoriaPostTabs->description);
												$corCategoriaPostTabs = $separarBanner[0];
												$linkCategoria = get_category_link($categoriaPostTabs->cat_ID);
											}
											$miniaturaPostTab = rwmb_meta('Tatigodoy_imagem_post_normal');
											$miniaturaPostTab = $miniaturaPostTab[0];
											
											if($miniaturaPost){
												$imagemPost = $miniaturaPost[0];
											}else{
												$imagemPost = $imagemPostTabs;
											}
								 	?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPostTabs; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPostTabs; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>
					<div class="verMaisPosts">
						<span class="vermais"><a href="<?php echo $linkCategoria; ?>">Ver mais Posts</a></span>
					</div>
				</div>	

				<div class="listaDePosts tab_cont" id="2">
					<div class="row">
						<?php 
						//LOOP DE POST POR CATEGORIA		
						$postTabs = new WP_Query(array(
							'post_type'     => 'post',
							'posts_per_page'   => 10,
							'tax_query'     => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $slugsCategoria[1],
										)
									)
								)
							);
						?>
						<div class="col-md-12">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
										while($postTabs->have_posts()): $postTabs->the_post();
											$categoriaPostTabs = get_the_category();
											$imagemPostTabs = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemPostTabs = $imagemPostTabs[0];
											global $post;
											foreach ($categoriaPostTabs as $categoriaPostTabs) {
												$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPostTabs->cat_ID);
												$separarBanner = explode("|", $categoriaPostTabs->description);
												$corCategoriaPostTabs = $separarBanner[0];
												$linkCategoria = get_category_link($categoriaPostTabs->cat_ID);
											}
											$miniaturaPostTab = rwmb_meta('Tatigodoy_imagem_post_normal');
											$miniaturaPostTab = $miniaturaPostTab[0];
											
											if($miniaturaPost){
												$imagemPost = $miniaturaPost[0];
											}else{
												$imagemPost = $imagemPostTabs;
											}
								 	?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPostTabs; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPostTabs; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>
					<div class="verMaisPosts">
						<span class="vermais"><a href="<?php echo $linkCategoria; ?>">Ver mais Posts</a></span>
					</div>
				</div>	

				<div class="listaDePosts tab_cont" id="3">
					<div class="row">
						<?php 
						//LOOP DE POST POR CATEGORIA		
						$postTabs = new WP_Query(array(
							'post_type'     => 'post',
							'posts_per_page'   => 10,
							'tax_query'     => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $slugsCategoria[2],
										)
									)
								)
							);
						?>
						<div class="col-md-12">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
										while($postTabs->have_posts()): $postTabs->the_post();
											$categoriaPostTabs = get_the_category();
											$imagemPostTabs = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemPostTabs = $imagemPostTabs[0];
											global $post;
											foreach ($categoriaPostTabs as $categoriaPostTabs) {
												$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPostTabs->cat_ID);
												$separarBanner = explode("|", $categoriaPostTabs->description);
												$corCategoriaPostTabs = $separarBanner[0];
												$linkCategoria = get_category_link($categoriaPostTabs->cat_ID);
											}
											$miniaturaPostTab = rwmb_meta('Tatigodoy_imagem_post_normal');
											$miniaturaPostTab = $miniaturaPostTab[0];
											
											if($miniaturaPost){
												$imagemPost = $miniaturaPost[0];
											}else{
												$imagemPost = $imagemPostTabs;
											}
								 	?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPostTabs; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPostTabs; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>
					<div class="verMaisPosts">
						<span class="vermais"><a href="<?php echo $linkCategoria; ?>">Ver mais Posts</a></span>
					</div>
				</div>	

				<div class="listaDePosts tab_cont" id="4">
					<div class="row">
						<?php 
						//LOOP DE POST POR CATEGORIA		
						$postTabs = new WP_Query(array(
							'post_type'     => 'post',
							'posts_per_page'   => 10,
							'tax_query'     => array(
								array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $slugsCategoria[3],
										)
									)
								)
							);
						?>
						<div class="col-md-12">
							<div class="listaDePostsDireita">
								<ul>
									<?php 
										while($postTabs->have_posts()): $postTabs->the_post();
											$categoriaPostTabs = get_the_category();
											$imagemPostTabs = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
											$imagemPostTabs = $imagemPostTabs[0];
											global $post;
											foreach ($categoriaPostTabs as $categoriaPostTabs) {
												$imagemCategoriaDestaque = z_taxonomy_image_url($categoriaPostTabs->cat_ID);
												$separarBanner = explode("|", $categoriaPostTabs->description);
												$corCategoriaPostTabs = $separarBanner[0];
												$linkCategoria = get_category_link($categoriaPostTabs->cat_ID);
											}
											$miniaturaPostTab = rwmb_meta('Tatigodoy_imagem_post_normal');
											$miniaturaPostTab = $miniaturaPostTab[0];
											
											if($miniaturaPost){
												$imagemPost = $miniaturaPost[0];
											}else{
												$imagemPost = $imagemPostTabs;
											}
								 	?>
									<li>
										<article>
											<a href="<?php echo get_permalink() ?>">
												<div class="postHover" style="background: <?php echo $corCategoriaPostTabs; ?>;">
													<span class="dataPostHover"><?php echo  get_the_date('j M Y'); ?></span>
													<h1 class="tituloPostHover"><?php echo get_the_title(); ?></h1>
													<p class="descricaoPostHover"><?php customExcerpt(100); ?></p>
													<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
												</div>	
												<figure class="imagemDestaquePost" style="background: url(<?php echo $imagemPost; ?>)">
													<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
												</figure>
												<div class="informacoesPost" style="background: <?php echo $corCategoriaPostTabs; ?>">
													<div class="row">
														<div class="col-xs-3">
															<div class="iconePost">
																<img src="<?php echo $imagemCategoriaDestaque; ?>" alt="<?php echo get_the_title(); ?>">
															</div>
														</div>
														<div class="col-xs-9">
															<div class="detalhesPost">
																<h1 class="tituloPost"><?php echo get_the_title(); ?></h1>
																<h4 class="dataPost"><?php echo  get_the_date('j M Y'); ?></h4>
															</div>
														</div>
													</div>
												</div>
											</a>
										</article>
									</li>
								<?php endwhile ; wp_reset_query();?>
								</ul>
							</div>
						</div>
					</div>
					<div class="verMaisPosts">
						<span class="vermais"><a href="<?php echo $linkCategoria; ?>">Ver mais Posts</a></span>
					</div>
				</div>	

			</div>
		</section>

		<section class="instagramFeed">
			<div class="containerFull">
				<h2>@tatigodoy No instagram!</h2>
				<div class="imagensInstagram">
					<?php echo do_shortcode('[instagram-feed]'); ?>
				</div>
			</div>
		</section>
	</div>

<?php 
get_footer();