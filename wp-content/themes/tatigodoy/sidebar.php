<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tatigodoy
 */
global $configuracao;

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="sidebarPost">

	<div class="estanteVirtual">
		<a href="<?php echo $configuracao['pg_post_estante_virtual_link'] ?>">
			<figure>
				<img src="<?php echo $configuracao['pg_post_estante_virtual_img']['url'] ?>" alt="Estante virtual">
			</figure>
		</a>
	</div>

	<div class="inscrevaSeCurso">
		<a href="<?php echo $configuracao['pg_post_curso_link'] ?>" target="_blank">
			<figure>
				<img src="<?php echo $configuracao['pg_post_curso_img']['url'] ?>" alt="Curso">
			</figure>
		</a>
	</div>

	<div class="youtubePost">
		<span>Acompanhe meus videos</span>

		<script src="https://apis.google.com/js/platform.js"></script>

		<div class="g-ytsubscribe" data-channelid="UCBlBXUL1EuSFQX_u1LJ_yNw" data-layout="full" data-count="default"></div>
	</div>

	<div class="facebookPost">
		<span class="curtaPagina">Curta nossa página</span>
		
		<div class="fb-page" data-href="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FacilitaOrganizacao/?hc_ref=ARTHPDSyAuOHCcJrNQSlzUg3yUTHUfj6kNWCBxNJEPqLpP2z02ezxhgG6u95Xia7k8I&amp;fref=nf&amp;__xts__[0]=68.ARAIbYYiRoWELW6Seq4E6qKzY1UiHpHScGxFkY2JC_Qq9jc-5xVQOoh6PMWQZUySrZiXxwIKdjznZGFgfxgkgSozGuUNlciFDFztYRaJZ14NY8boi4W0yCi3iCTHB_HHy5iLl9pazKAykFf8EFVRtfgYdzQZ-_Z6eX7oiUyXkUd84BwsRI3H9Q&amp;__tn__=kC-R">Facilita Organização/Tati Godoy</a></blockquote></div>
	</div>

	<div class="instagramSidebar">
		<span>Me Segue no insta :)</span>

		<figure>
			<?php echo do_shortcode('[instagram-feed cols=2 rows=5]'); ?>
		</figure>
	</div>

	
</div>